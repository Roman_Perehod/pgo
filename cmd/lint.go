package cmd

import (
	"github.com/spf13/cobra"
	"os/exec"
)

// lintCmd represents the lint command

//platoLinters := []string{"--vendor", "--disable-all", "--enable-gc", "--enable=vet", "--enable=vetshadow", "--enable=golint",
//			 "--enable=ineffassign", "--enable=goconst", "--enable=structcheck", "--enable=aligncheck", "--enable=deadcode", "--enable=gocyclo",
//			 "--enable=gosimple", "--enable=unconvert", "--tests", "--deadline=120s", "--exclude=modstatus_string"}

var lintCmd = &cobra.Command{
	Use:   "lint",
	Short: "pgo lint [path] calls gometalinter",
	Long:  `pgo lint [path] calls gometalinter with standard Plato linters.`,
	Run: func(cmd *cobra.Command, args []string) {
		var argsWithoutProg = args
		_, err := exec.LookPath("gometalinter")
		if err != nil {
			StartWithStandardOutput("go", "get", "-u", "github.com/alecthomas/gometalinter")
		}
		if len(argsWithoutProg) == 0 {
			argsWithoutProg = append(argsWithoutProg, "./...")
		}
		if len(argsWithoutProg) > 0 {
			if argsWithoutProg[0] == "." {
				argsWithoutProg[0] = "./..."
			}
		}
		platoLinters := []string{"--vendor", "--disable-all", "--enable-gc", "--enable=vet", "--enable=vetshadow",
			"--enable=ineffassign",
			 "--enable=unconvert", "--deadline=120s", "--exclude=[a-z]*_test"}

		argsWithoutProg = append(argsWithoutProg, platoLinters...)
		StartWithStandardOutput("gometalinter", argsWithoutProg...)
	},
}

func init() {
	RootCmd.AddCommand(lintCmd)
}
