package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os/exec"
	"time"
)

var cfgFile string

//we configure ldflags on initialize to further build
var ldflags string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "pgo",
	Short: "Tool to make plato golang workflow easier",
	Long:  `Tool to make plato golang workflow easier`,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.pgo.yaml)")

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}
	RootCmd.DisableFlagParsing = true
	viper.SetConfigName(".pgo")  // name of config file (without extension)
	viper.AddConfigPath("$HOME") // adding home directory as first search path
	viper.AutomaticEnv()         // read in environment variables that match

	var (
		cmdOut []byte
		err    error
	)
	cmdName := "git"
	cmdArgs := []string{"rev-parse", "--verify", "HEAD"}
	if cmdOut, err = exec.Command(cmdName, cmdArgs...).Output(); err != nil {
		fmt.Println("Warning:  It seems that your current directory is not under the git control")
		fmt.Println("If you are going to build something, please change directory to the project")
	} else {
		//We need to get rid of \n at the end of the output of "git rev-parse"
		cmdOut = cmdOut[:len(cmdOut)-2]
		sha := string(cmdOut)
		builddate := time.Now().UTC().Format(time.UnixDate)
		ldflags = fmt.Sprintf("-X \"main.commit=%s\" -X \"main.builddate=%s\"", sha, builddate)
	}
	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
