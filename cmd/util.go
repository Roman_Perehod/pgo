package cmd

import (
	"log"
	"os"
	"os/exec"
)


//StartWithStandardOutput is a utiility function to execute command and direct messages to standard output
func StartWithStandardOutput(name string, arg ...string) {
	command := exec.Command(name, arg...)
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	if err := command.Run(); err != nil {
		log.Fatal(err)
	}
}
